export default interface Distribuidora {
    id?: number | any;
    estado: {
        id: number;
        pais: {
            id: number;
            nome: string;
            sigla: string;
        },
        nome: string;
        uf: string;
    } | any;
    name: string | any;
    is_active: boolean | any;
    timestamp?: Date | any;
    created_by?: number | any;
}