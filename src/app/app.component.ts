import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  template: `
  <main class="app-main">
        <trainee-navbar></trainee-navbar>
        <article class="app-container">
          <div class="app-content">
            <trainee-config></trainee-config>
          </div>
        </article>
    </main>
  `,
})

export class AppComponent {
  constructor(private http: HttpClient) {
    //Test connection to server
    this.http.get('api/distribuidoras').subscribe(console.log)
  }
}
