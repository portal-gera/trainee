// Karma configuration file, see link for more information
// https://karma-runner.github.io/1.0/config/configuration-file.html

module.exports = function(config)
{
    config.set(
    {
        basePath: '',
        frameworks: ['jasmine', '@angular-devkit/build-angular'],
        plugins: [
            require('karma-jasmine'),
            require('karma-chrome-launcher'),
            require('karma-coverage-istanbul-reporter'),
            require('karma-summary-reporter'),
            require('@angular-devkit/build-angular/plugins/karma')
        ],
        client:
        {
            clearContext: false // leave Jasmine Spec Runner output visible in browser
        },
        coverageIstanbulReporter:
        {
            dir: require('path')
                .join(__dirname, '../coverage'),
            reports: ['html', 'lcovonly'],
            fixWebpackSourcePaths: true,
            thresholds:
            {
                statements: 80,
                lines: 80,
                branches: 80,
                functions: 80
            }
        },
        reporters: ['progress', 'summary'],
        summaryReporter:
        {
            // 'failed', 'skipped' or 'all'
            show: 'failed',
            // Limit the spec label to this length
            specLength: 50,
            // Show an 'all' column as a summary
            overviewColumn: true
        },
        port: 9876,
        colors: true,
        logLevel: config.LOG_ERROR,
        browsers: ['ChromeHeadless'],
        autoWatch: true,
        singleRun: false
    });
};